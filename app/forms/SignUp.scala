package forms

/**
  * Created by araki on 2017/10/21.
  */
case class SignUp(
    name: String,
    email: String,
    password: String,
    confirm: String
)
