import com.google.inject.AbstractModule
import services._

/**
  * Created by araki on 2017/10/15.
  */
class AppModule extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[UserService]).to(classOf[UserServiceImpl])
    bind(classOf[MicroPostService]).to(classOf[MicroPostServiceImpl])
    bind(classOf[UserFollowService]).to(classOf[UserFollowServiceImpl])
  }

}
