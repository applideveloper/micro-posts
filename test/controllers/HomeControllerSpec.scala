package controllers

import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.FakeRequest
import play.api.test.Helpers._
import services.UserService

/**
  * Created by araki on 2017/10/14.
  */
class HomeControllerSpec extends PlayFunSpec with GuiceOneAppPerSuite {

  object Config extends AuthConfigSupport {
    override val userService: UserService = app.injector.instanceOf[UserService]
  }

  describe("HomeController") {
    describe("route of HomeController#index") {
      it("should be valid") {
        val result = route(
          app,
          addCsrfToken(
            FakeRequest(
              GET,
              routes.HomeController.index().toString
            )
          )
        ).get
        status(result) mustBe OK
      }

      it("should be valid when logged in") { // ログインの時のテスト
        val email = "test@test.com"
        val result =
          route(
            app,
            addCsrfToken(
              FakeRequest(GET, routes.HomeController.index().toString)
            )
          ).get
        status(result) mustBe OK
        contentAsString(result) must include(email)
      }
    }
  }
}
